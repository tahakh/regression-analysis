# Topics : FVC Scatterplot Linear regression
# Creator : Tahmina Akhter
# Created : 25th September, 2018

#load library 
library(lattice) # For levelplot function
library(grid)
library(gridExtra)
library(gtable)
library(ggplot2)
library(latticeExtra)
library(reshape)
library(ggpubr)

# Clear the Environment 
rm(list=ls())

############################
# Load Severe Pvalue matrix
############################

load('Result/Clinical Data_ Linear Regression/2018-04-23/PvalSTDE_steroid_Severe.RData')
load('Result/Clinical Data_ Linear Regression/2018-04-23/PvalSTDE_placebo_Severe.RData')

pval_mat_placebo <- t(pval_mat_placebo)
pval_mat_steroid <- t(pval_mat_steroid)

FVC_response <- cbind(pval_mat_placebo[,c(7:9)], pval_mat_steroid[,c(7:9)])
colnames(FVC_response) <- c("Placebo_pval","Placebo_std.Err","Placebo_Slope","Steroid_pval","Steroid_std.Err","Steroid_Slope")

FVC_response_order <- FVC_response[order(FVC_response[,6] - FVC_response[,3], decreasing = TRUE),]


FVC_Severe <- matrix("", nrow = 0, ncol = 6)
colnames(FVC_Severe) <- c("Biomarker", "Effect", "Response", "Slope", "Pvalue", "Std.Err")

for(i in 1:nrow(FVC_response_order)) {
  
  FVC_Severe <- rbind(FVC_Severe, c(rownames(FVC_response_order)[i], paste(rownames(FVC_response_order)[i], colnames(FVC_response_order)[3], sep = "_"), paste("Placebo"), FVC_response_order[i, "Placebo_Slope"], FVC_response_order[i, c(1,2)]))
  FVC_Severe <- rbind(FVC_Severe, c(rownames(FVC_response_order)[i], paste(rownames(FVC_response_order)[i], colnames(FVC_response_order)[6], sep = "_"), paste("Steroid"), FVC_response_order[i, "Steroid_Slope"], FVC_response_order[i, c(4,5)]))
  
}

FVC_Severe <- as.data.frame(FVC_Severe)
FVC_Severe[,'Slope'] <- as.numeric(as.character(FVC_Severe[,'Slope']))
FVC_Severe[,'Pvalue'] <- as.numeric(as.character(FVC_Severe[,'Pvalue']))
FVC_Severe[,'Std.Err'] <- as.numeric(as.character(FVC_Severe[,'Std.Err']))
FVC_Severe[,'Response'] <- as.character(FVC_Severe[,'Response'])
FVC_Severe[,'Biomarker'] <- as.character(FVC_Severe[,'Biomarker'])
FVC_Severe[,'Effect'] <- as.character(FVC_Severe[,'Effect'])



# Add fourth column to point biomarkers line type
# fourth_col <- unlist(lapply(strsplit(as.character(FVC_Severe[ ,"Effect"]), split = "_"), function(x) {x <- x[-c(length(x), length(x)-1)]; return(paste(x, collapse = "_"))}))
# FVC_Severe <- cbind(FVC_Severe, fourth_col)



g1 <-  ggplot(FVC_Severe, aes(x = Effect,y = Slope, group= Biomarker)) + 
  geom_point(aes(color = Response, size = -log10(Pvalue))) + theme_bw() +
  labs (title = "Severe Response Effects FVC", x = "Biomarkers",y = " Slope ") +
  theme( axis.text.x= element_text(angle = 90, hjust = 1)) +
  theme(legend.title = element_text(face = "bold"))+ 
  scale_x_discrete(limits= FVC_Severe$Biomarkers)+ 
  geom_errorbar(aes(ymin=Slope-Std.Err, ymax=Slope+Std.Err), width=0.5, color="#cd940c") +
  geom_line(aes(group= Biomarker))

############################
# Load Mild Pvalue matrix
############################

load('Result/Clinical Data_ Linear Regression/2018-04-23/PvalSTDE_steroid_Mild.RData')
load('Result/Clinical Data_ Linear Regression/2018-04-23/PvalSTDE_placebo_Mild.RData')

pval_mat_placebo <- t(pval_mat_placebo)
pval_mat_steroid <- t(pval_mat_steroid)

FVC_response <- cbind(pval_mat_placebo[,c(7:9)], pval_mat_steroid[,c(7:9)])
colnames(FVC_response) <- c("Placebo_pval","Placebo_std.Err","Placebo_Slope","Steroid_pval","Steroid_std.Err","Steroid_Slope")

FVC_response_order <- FVC_response[order(FVC_response[,6] - FVC_response[,3], decreasing = TRUE),]


FVC_Mild <- matrix("", nrow = 0, ncol = 6)
colnames(FVC_Mild) <- c("Biomarker", "Effect", "Response", "Slope", "Pvalue", "Std.Err")

for(i in 1:nrow(FVC_response_order)) {
  
  FVC_Mild <- rbind(FVC_Mild, c(rownames(FVC_response_order)[i], paste(rownames(FVC_response_order)[i], colnames(FVC_response_order)[3], sep = "_"), paste("Placebo"), FVC_response_order[i, "Placebo_Slope"], FVC_response_order[i, c(1,2)]))
  FVC_Mild <- rbind(FVC_Mild, c(rownames(FVC_response_order)[i], paste(rownames(FVC_response_order)[i], colnames(FVC_response_order)[6], sep = "_"), paste("Steroid"), FVC_response_order[i, "Steroid_Slope"], FVC_response_order[i, c(4,5)]))
  
}

FVC_Mild <- as.data.frame(FVC_Mild)
FVC_Mild[,'Slope'] <- as.numeric(as.character(FVC_Mild[,'Slope']))
FVC_Mild[,'Pvalue'] <- as.numeric(as.character(FVC_Mild[,'Pvalue']))
FVC_Mild[,'Std.Err'] <- as.numeric(as.character(FVC_Mild[,'Std.Err']))
FVC_Mild[,'Response'] <- as.character(FVC_Mild[,'Response'])
FVC_Mild[,'Biomarker'] <- as.character(FVC_Mild[,'Biomarker'])
FVC_Mild[,'Effect'] <- as.character(FVC_Mild[,'Effect'])


g2 <-  ggplot(FVC_Mild, aes(x = Effect,y = Slope, group= Biomarker)) + 
  geom_point(aes(color = Response, size = -log10(Pvalue))) + theme_bw() +
  labs (title = "Mild Response Effects FVC", x = "Biomarkers",y = " Slope ") +
  theme( axis.text.x= element_text(angle = 90, hjust = 1)) +
  theme(legend.title = element_text(face = "bold"))+ 
  scale_x_discrete(limits= FVC_Mild$Biomarkers)+ 
  geom_errorbar(aes(ymin=Slope-Std.Err, ymax=Slope+Std.Err), width=0.5, color="#cd940c") +
  geom_line(aes(group= Biomarker))

############################
# Load COPD Pvalue matrix
############################

load('Result/Clinical Data_ Linear Regression/2018-04-23/PvalSTDE_steroid_COPD.RData')
load('Result/Clinical Data_ Linear Regression/2018-04-23/PvalSTDE_placebo_COPD.RData')

pval_mat_placebo <- t(pval_mat_placebo)
pval_mat_steroid <- t(pval_mat_steroid)

FVC_response <- cbind(pval_mat_placebo[,c(7:9)], pval_mat_steroid[,c(7:9)])
colnames(FVC_response) <- c("Placebo_pval","Placebo_std.Err","Placebo_Slope","Steroid_pval","Steroid_std.Err","Steroid_Slope")

FVC_response_order <- FVC_response[order(FVC_response[,6] - FVC_response[,3], decreasing = TRUE),]


FVC_COPD <- matrix("", nrow = 0, ncol = 6)
colnames(FVC_COPD) <- c("Biomarker", "Effect", "Response", "Slope", "Pvalue", "Std.Err")

for(i in 1:nrow(FVC_response_order)) {
  
  FVC_COPD <- rbind(FVC_COPD, c(rownames(FVC_response_order)[i], paste(rownames(FVC_response_order)[i], colnames(FVC_response_order)[3], sep = "_"), paste("Placebo"), FVC_response_order[i, "Placebo_Slope"], FVC_response_order[i, c(1,2)]))
  FVC_COPD <- rbind(FVC_COPD, c(rownames(FVC_response_order)[i], paste(rownames(FVC_response_order)[i], colnames(FVC_response_order)[6], sep = "_"), paste("Steroid"), FVC_response_order[i, "Steroid_Slope"], FVC_response_order[i, c(4,5)]))
  
}

FVC_COPD <- as.data.frame(FVC_COPD)
FVC_COPD[,'Slope'] <- as.numeric(as.character(FVC_COPD[,'Slope']))
FVC_COPD[,'Pvalue'] <- as.numeric(as.character(FVC_COPD[,'Pvalue']))
FVC_COPD[,'Std.Err'] <- as.numeric(as.character(FVC_COPD[,'Std.Err']))
FVC_COPD[,'Response'] <- as.character(FVC_COPD[,'Response'])
FVC_COPD[,'Biomarker'] <- as.character(FVC_COPD[,'Biomarker'])
FVC_COPD[,'Effect'] <- as.character(FVC_COPD[,'Effect'])


g3 <-  ggplot(FVC_COPD, aes(x = Effect,y = Slope, group= Biomarker)) + 
  geom_point(aes(color = Response, size = -log10(Pvalue))) + theme_bw() +
  labs (title = "COPD Response Effects FVC", x = "Biomarkers",y = " Slope ") +
  theme( axis.text.x= element_text(angle = 90, hjust = 1)) +
  theme(legend.title = element_text(face = "bold"))+ 
  scale_x_discrete(limits= FVC_COPD$Biomarkers)+ 
  geom_errorbar(aes(ymin=Slope-Std.Err, ymax=Slope+Std.Err), width=0.5, color="#cd940c") +
  geom_line(aes(group= Biomarker))


############################
# Load All Pvalue matrix
############################

load('Result/Clinical Data_ Linear Regression/2018-04-23/PvalSTDE_steroid_All.RData')
load('Result/Clinical Data_ Linear Regression/2018-04-23/PvalSTDE_placebo_All.RData')

pval_mat_placebo <- t(pval_mat_placebo)
pval_mat_steroid <- t(pval_mat_steroid)

FVC_response <- cbind(pval_mat_placebo[,c(7:9)], pval_mat_steroid[,c(7:9)])
colnames(FVC_response) <- c("Placebo_pval","Placebo_std.Err","Placebo_Slope","Steroid_pval","Steroid_std.Err","Steroid_Slope")

FVC_response_order <- FVC_response[order(FVC_response[,6] - FVC_response[,3], decreasing = TRUE),]


FVC_All <- matrix("", nrow = 0, ncol = 6)
colnames(FVC_All) <- c("Biomarker", "Effect", "Response", "Slope", "Pvalue", "Std.Err")

for(i in 1:nrow(FVC_response_order)) {
  
  FVC_All <- rbind(FVC_All, c(rownames(FVC_response_order)[i], paste(rownames(FVC_response_order)[i], colnames(FVC_response_order)[3], sep = "_"), paste("Placebo"), FVC_response_order[i, "Placebo_Slope"], FVC_response_order[i, c(1,2)]))
  FVC_All <- rbind(FVC_All, c(rownames(FVC_response_order)[i], paste(rownames(FVC_response_order)[i], colnames(FVC_response_order)[6], sep = "_"), paste("Steroid"), FVC_response_order[i, "Steroid_Slope"], FVC_response_order[i, c(4,5)]))
  
}

FVC_All <- as.data.frame(FVC_All)
FVC_All[,'Slope'] <- as.numeric(as.character(FVC_All[,'Slope']))
FVC_All[,'Pvalue'] <- as.numeric(as.character(FVC_All[,'Pvalue']))
FVC_All[,'Std.Err'] <- as.numeric(as.character(FVC_All[,'Std.Err']))
FVC_All[,'Response'] <- as.character(FVC_All[,'Response'])
FVC_All[,'Biomarker'] <- as.character(FVC_All[,'Biomarker'])
FVC_All[,'Effect'] <- as.character(FVC_All[,'Effect'])


g4 <-  ggplot(FVC_All, aes(x = Effect,y = Slope, group= Biomarker)) + 
  geom_point(aes(color = Response, size = -log10(Pvalue))) + theme_bw() +
  labs (title = "All Response Effects FVC", x = "Biomarkers",y = " Slope ") +
  theme( axis.text.x= element_text(angle = 90, hjust = 1)) +
  theme(legend.title = element_text(face = "bold"))+ 
  scale_x_discrete(limits= FVC_All$Biomarkers)+ 
  geom_errorbar(aes(ymin=Slope-Std.Err, ymax=Slope+Std.Err), width=0.5, color="#cd940c") +
  geom_line(aes(group= Biomarker))

# Extracxt the legend from g1
legend <- gtable_filter(ggplotGrob(g1), "Response")
# grid.draw(legend)    # Make sure the legend has been extracted

# One way to present figure
# grid.arrange(g1,g2,g3,g4, nrow = 2, ncol= 2)

# 2nd way to present figure
figure <- ggarrange(g1,g2,g3,g4, common.legend = TRUE)
annotate_figure(figure, top = textGrob("Model P-value response effects", 
                                       vjust = 1, gp = gpar(fontface = "bold", cex = 1,col = "blue")))




